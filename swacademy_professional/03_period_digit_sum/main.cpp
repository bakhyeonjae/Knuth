#include <iostream>

using namespace std;

typedef unsigned long UL;

int getLength(UL num)
{
    int length = 1;
    UL divisor = 10;
    
    while (num / divisor != 0) {
        length++;
        divisor *= 10;
    }

    return length;
}

int getDigit(UL num, int pos)
{
    UL divisor = 10;
    
    for (int i = 0 ; i < pos ; i++ )
        divisor *= 10;

    num %= divisor;
    divisor /= 10;
    num /= divisor;

    return int(num);
}

UL sumDigitAtPositionBF(UL b, UL e, int pos)
{
    UL sum = 0;

    for (UL i = b ; i <= e ; i++)
        sum += getDigit(i,pos);

    return sum;
}

UL pattern_sum_table[15] = {45,
                            450,
                            4500,
                            45000,
                            450000,
                            4500000,
                            45000000,
                            450000000,
                            4500000000,
                            45000000000,
                            450000000000,
                            4500000000000,
                            45000000000000,
                            450000000000000,
                            4500000000000000};

UL calcLowerSum(UL b, UL c, int pos)
{
    UL sum = 0;

    UL pattern_size = 1;
    for (int i = 0 ; i < pos ; i++) pattern_size *= 10;
    
    UL period = c - b;

    for ( int i = 9 ; period > 0 ; i-- ) {
        if (period < pattern_size) {
            sum += period * i;
            period = 0;
        } else {
            sum += pattern_size * i;
            period -= pattern_size;
        }
    }

    return sum;
}

UL calcUpperSum(UL f, UL e, int pos) 
{
    UL sum = 0;

    UL pattern_size = 1;
    for (int i = 0 ; i < pos ; i++ ) pattern_size *= 10;

    UL period = e - f + 1;

    for (int i = 0 ; period > 0 ; i++ ) {
        if (period < pattern_size) {
            sum += period * i;
            period = 0;
        } else {
            sum += pattern_size * i;
            period -= pattern_size;
        }
    }

    return sum;
}

UL calcSumPeriod(UL b, UL e, int pos) 
{
    UL pattern_size = 1;
    for (int i = 0 ; i < pos ; i++) pattern_size *= 10;

    UL sum = 0;
    
    int digit_b = getDigit(b,pos);
    int digit_e = getDigit(e,pos);

    if (digit_b == digit_e) {
        sum += (e-b+1) * digit_b;
    } else {
        UL ceil = (b/pattern_size + 1) * pattern_size;
        UL floor = (e/pattern_size) * pattern_size;

        sum += (ceil - b) * digit_b;
        sum += (e - floor + 1) * digit_e;

        for ( int i = digit_b + 1 ; i < digit_e ; i++ )
            sum += i * pattern_size;
    }

    return sum;
}

UL sumDigitAtPosition(UL b, UL e, int pos) 
{
    UL sum = 0;
    UL limitter = 10;

    for (int i = 0 ; i < pos ; i++) limitter *= 10;

    UL beginning_index = b % limitter;
    UL ceil_index = b + (beginning_index == 0 ? beginning_index : (limitter - beginning_index));

    UL floor_index = e / limitter * limitter;

    if (floor_index >= ceil_index) {

        UL pattern_period = (floor_index - ceil_index) / limitter;
        sum += pattern_sum_table[pos] * pattern_period;
        sum += calcLowerSum(b,ceil_index,pos);
        sum += calcUpperSum(floor_index,e,pos);
    } else {
        sum += calcSumPeriod(b,e,pos);
    }

    return sum;
}

typedef struct {
    UL b;
    UL e;
} Period;

#define TEST_SUCCESS -1

int regressionTest(Period *num_array, int num) {
    int error_code = TEST_SUCCESS;

    for (int i = 0; i < num ; i++ ) {
        UL b = num_array[i].b;
        UL e = num_array[i].e;

        int blen = getLength(b);
        int elen = getLength(e);
        int len = (blen > elen) ? blen : elen;

        UL sum = 0;
        for (int pos = 0 ; pos < len ; pos++) sum += sumDigitAtPosition(b,e,pos);

        UL sumBF = 0;
        for (int pos = 0 ; pos < len ; pos++) sumBF += sumDigitAtPositionBF(b,e,pos);

        if (sum != sumBF) return i;
    }

    return error_code;
}

void initTestValues(Period *period, int n)
{
    period[0].b = 0;        period[0].e = 10;
    period[1].b = 3;        period[1].e = 8;
    period[2].b = 4;        period[2].e = 5;
    period[3].b = 8;        period[3].e = 12;
    period[4].b = 11;        period[4].e = 13;
    period[5].b = 15;        period[5].e = 122;
    period[6].b = 11;        period[6].e = 99;
    period[7].b = 18;        period[7].e = 1928;
    period[8].b = 98;        period[8].e = 99;
    period[9].b = 0;        period[9].e = 1;
    period[10].b = 3;       period[10].e = 98723;
    period[11].b = 18;       period[11].e = 12534;
    period[12].b = 33;       period[12].e = 12345;
    period[13].b = 56;       period[13].e = 98721;
    period[14].b = 120;       period[14].e = 121;
    period[15].b = 342;       period[15].e = 9999;
    period[16].b = 11;       period[16].e = 3456;
    period[17].b = 1;       period[17].e = 19998;
    period[18].b = 2;       period[18].e = 39;
    period[19].b = 19999;       period[19].e = 20001;
}

#define TEST

int main(int argc, char *argv[]) {
#ifdef TEST
    UL b,e;
    Period periods[20];
    initTestValues(periods,20);
    if (TEST_SUCCESS == regressionTest(periods,20))
        cout << "The optimised algorithm is compared with brute force algorithm and verified." << endl;
#else
    int T;
    UL b, e;
    
    cin >> T;
    
    for ( int i = 0 ; i < T ; i++ ) {
        cin >> b;
        cin >> e;

        int blen = getLength(b);
        int elen = getLength(e);
        int len = (blen > elen) ? blen : elen;

        UL sum = 0;
        
        for ( int pos = 0 ; pos < len ; pos++ ) sum += sumDigitAtPosition(b,e,pos);
        
        cout << "#" << i << " " << sum << endl;
    }
#endif
}
