#include <iostream>
#include <limits>
#include <fstream>
using namespace std;

#define MAX_N 100000
const int INT_MAX_LIMIT = numeric_limits<int>::max();

// Child
typedef struct child Child;
struct child {
	int mNum;
	Child* mNext;
};

// Child 정보만 있는 Node Class
class Node
{
	Child *mFront;
	Child *mRear;
	Child *mCurChild;

public:
	int mSize;
	int mDepth;
	bool mVisited;


public:
	Node();
	~Node();

	void addChild(int num);
	//int getNextChildValue();
	int getCurChildValue();
	int getCurChildValueAndNext();
	int getRearChildValue();
	//void goFirstChild();
	int goNextChildLinear();	// 끝에 도달하면 0 반환, 그외 1 반환

	void init();
};

Node::Node() {
	mFront = NULL;
	mRear = NULL;
	mCurChild = NULL;
	mSize = 0;
	mVisited = false;
	mDepth = -1;
}

Node::~Node()
{
	Child* del;
	Child* tmp;
	int i;

	for (i = 0, del = mFront; i < mSize; i++) {
		tmp = del->mNext;
		delete del;
		del = tmp;
	}
}

void Node::addChild(int num) {
	Child* n = new Child;
	n->mNum = num;
	n->mNext = NULL;
	
	if (mSize == 0) {
		mFront = n;
		
		mCurChild = mFront;
	}
	else {
		
		mRear->mNext = n;

	}
	mRear = n;
	mSize++;
}

/*
int Node::getNextChildValue() {

	if (mFront == NULL) {
		cerr << this << " don't have a child." << endl;
		return 0;
	}

	if (mCurChild) {
		Child* tmp = mCurChild;
		mCurChild = mCurChild->mNext;
		return tmp->mNum;
	}

	mCurChild = mFront;
	return mCurChild->mNum;
}
*/
int Node::getCurChildValue() {
	if (mFront == NULL) {
		cerr << this << " don't have a child." << endl;
		return 0;
	}

	if (mCurChild == NULL) {
		cerr << "Current Child is not pointed in " << this << " node." << endl;
		return -1;
	}

	return mCurChild->mNum;
}

int Node::getCurChildValueAndNext() {
	
	int cur_value = getCurChildValue();

	if (mCurChild->mNext)
		mCurChild = mCurChild->mNext;
	else
		mCurChild = mFront;

	return cur_value;
}

int Node::getRearChildValue() {
	if (mRear == NULL) {
		cerr << this << " don't have a child." << endl;
		return 0;
	}

	return mRear->mNum;
}

int Node::goNextChildLinear() {
	if (mCurChild) {
		mCurChild = mCurChild->mNext;

		if (mCurChild == NULL) {
			mCurChild = mFront;
			return 0;

		}

		return 1;
	}

	return 0;
}

/*
void Node::goFirstChild() {
	mCurChild = mFront;
}
*/

void Node::init() {
	Child* del;
	Child* tmp;
	int i;

	for (i = 0, del = mFront; i < mSize; i++) {
		tmp = del->mNext;
		delete del;
		del = tmp;
	}

	mFront = NULL;
	mRear = NULL;
	mCurChild = NULL;
	mSize = 0;
	mVisited = false;
	mDepth = -1;
}


// 스택 구현
class Stack
{
	int mStack[MAX_N * 2];
	int mRear;


public:
	Stack();
	//~Stack();

	bool isEmpty();
	bool isFull();
	bool push(int n);
	bool pop(int& n);
	void init();
};

Stack::Stack() {
	mRear = -1;
}

bool Stack::isEmpty() {
	if (mRear == -1) return true;
	return false;
}

bool Stack::isFull() {
	if (mRear == (MAX_N * 2) - 1) return true;
	return false;
}

bool Stack::push(int n) {
	if (isFull()) {
		cerr << this << " stack is full." << endl;
		return false;
	}

	mRear++;
	mStack[mRear] = n;

	return true;
}

bool Stack::pop(int& n) {
	if (isEmpty()) {
		cerr << this << " stack is empty." << endl;
		return false;
	}

	n = mStack[mRear];
	mRear--;

	return true;
}

void Stack::init()
{
	mRear = -1;
}


// 큐 구현
class Queue
{
	int mQ[MAX_N + 1];
	int mFront;
	int mRear;

	
public:
	Queue();
	~Queue();

	bool isEmpty();
	bool isFull();
	bool push(int n);
	bool pop(int& n);
	void init();
};

Queue::Queue()
{
	mFront = 0;
	mRear = 0;
}

Queue::~Queue()
{

}

bool Queue::push(int n)
{
	if (isFull()) {
		cerr << this << " queue is full." << endl;
		return false;
	}

	mRear = (mRear + 1) % (MAX_N + 1);
	mQ[mRear] = n;

	return true;
}

bool Queue::pop(int& n)
{
	if (isEmpty()) {
		cerr << this << " queue is empty." << endl;
		return false;
	}

	mFront = (mFront + 1) % (MAX_N + 1);
	n = mQ[mFront];

	return true;
}

bool Queue::isEmpty()
{
	if (mFront == mRear) return true;
	return false;
}

bool Queue::isFull()
{
	if ((mRear + 1) % (MAX_N + 1) == mFront) return true;
	return false;
}

void Queue::init()
{
	mFront = 0;
	mRear = 0;
}

class RMQ 
{
	int mArrSize;

	int* mRangeMin;

	int init(int arr[], int left, int right, int node);
	int min(int a, int b);

	int query(int left, int right, int node, int node_left, int node_right);
public:
	RMQ(int arr[], int size);
	~RMQ();

	int query(int left, int right);

};

RMQ::RMQ(int arr[], int size)
{
	mArrSize = size;
	mRangeMin = new int[mArrSize * 4];
	init(arr, 0, mArrSize - 1, 1);
}

RMQ::~RMQ() {
	delete[] mRangeMin;
}

int RMQ::init(int arr[], int left, int right, int node) {
	if (left == right) {
		return mRangeMin[node] = arr[left];
	}

	int mid = (left + right) / 2;
	int left_min = init(arr, left, mid, node * 2);
	int right_min = init(arr, mid + 1, right, node * 2 + 1);

	return mRangeMin[node] = min(left_min, right_min);
}

int RMQ::min(int a, int b) {
	return (a < b ? a : b);
}

int RMQ::query(int left, int right, int node, int node_left, int node_right) {
	if (right < node_left || left > node_right) return INT_MAX_LIMIT;

	if (left <= node_left && right >= node_right) return mRangeMin[node];

	int mid = (node_left + node_right) / 2;
	
	return min(query(left, right, node * 2, node_left, mid), query(left, right, node * 2 + 1, mid + 1, node_right));

}

int RMQ::query(int left, int right) {
	return query(left, right, 1, 0, mArrSize - 1);
}

Node tree[MAX_N];	// 입력 트리
int tree_num;		// 입력 노드 개수

Queue q;	// BFS를 위한 Q

Stack stk;	// traverse를 위한 stack


int trip[MAX_N * 2];	// RMQ 활용을 위한 일련번호 배열
int trip_num;			// 현재 trip 배열 개수

int no2serial[MAX_N];
int serial2no[MAX_N];
int next_serial;

int locInTrip[MAX_N];

/*
void traverse(int here)
{
	no2serial[here] = next_serial;
	serial2no[next_serial] = here;

	++next_serial;

	locInTrip[here] = trip_num;
	trip[trip_num] = no2serial[here];
	trip_num++;

	if (tree[here].getSize() > 0) {
		do {
			traverse(tree[here].getCurChildValue() - 1);
			trip[trip_num] = no2serial[here];
			trip_num++;
		} while (tree[here].goNextChildLinear());
	}
}
*/

void traverse_s()
{
	int here, d = 0;

	stk.push(0);

	while (!(stk.isEmpty())) {
		stk.pop(here);

		if (!(tree[here].mVisited)) {
			tree[here].mVisited = true;

			no2serial[here] = next_serial;
			serial2no[next_serial] = here;

			++next_serial;

			locInTrip[here] = trip_num;

			if (tree[here].mSize > 0) {
				do {
					stk.push(here);
					stk.push(tree[here].getCurChildValue() - 1);
				} while (tree[here].goNextChildLinear());
			}
		}

		trip[trip_num] = no2serial[here];
		trip_num++;
	}

	stk.init();
}

int getDistance(RMQ* rmq, int start, int end)
{
	int loc_start = locInTrip[start - 1];
	int loc_end = locInTrip[end - 1];
	int tmp;
	int LCA;

	if (loc_start > loc_end) {
		tmp = loc_start;
		loc_start = loc_end;
		loc_end = tmp;
	}

	LCA = serial2no[rmq->query(loc_start, loc_end)];

	return tree[start - 1].mDepth + tree[end - 1].mDepth - 2 * tree[LCA].mDepth;
}

__int64 getResultBFS()
{
	
	int node, last_node = 0;
	__int64 move_cnt = 0;
	int i;
	RMQ* rmq;

	// RMQ 생성
	next_serial = 0;
	trip_num = 0;
	traverse_s();
	rmq = new RMQ(trip, trip_num);
	

	// root 삽입
	q.push(1);

	while (!(q.isEmpty())) {
		q.pop(node);

		if (tree[node - 1].mSize > 0) {
			if (last_node) move_cnt += getDistance(rmq, last_node, node);
			
			move_cnt += (tree[node - 1].mSize * 2 - 1);

			for (i = 0; i < tree[node - 1].mSize; i++) {
				q.push(tree[node - 1].getCurChildValueAndNext());
			}

			last_node = tree[node - 1].getRearChildValue();


		}
	}

	// Q reset
	q.init();

	delete rmq;

	return move_cnt;
}

int main()
{
	int T, P;
	int i, j;

	FILE *stream;
	//FILE *stream_out;
	freopen_s(&stream, "C:\\study\\SC_H1605\\sample_input.txt", "r", stdin);
	//freopen_s(&stream_out, "C:\\study\\SC_H1605\\tw_output.txt", "w", stdout);
	//freopen_s(&stream, "C:\\study\\SC_H1605\\sample_input_36.txt", "r", stdin);

	//ofstream outFile("C:\\study\\SC_H1605\\sample_input_36.txt");



	cin >> T;

	for (i = 0; i < T; i++) {

		

		//cout << "*** " << i + 1 << "번째 ***" << endl;

		tree[0].mDepth = 0;

		cin >> tree_num;

		//if (i == 35)
		//outFile << tree_num << endl;

		// 입력 데이터를 통한 트리 구성
		for (j = 1; j < tree_num; j++) {
			// 각 노드에 대한 부모 노드 입력 받음.
			cin >> P;

			//if (i == 35)
			//outFile << P;

			//if (i == 35) {
				//if (j == tree_num - 1) outFile << endl;
				//else outFile << " ";
			//}

			// 해당 부모 노드에 현재 노드를 자식 노드로 추가
			tree[P-1].addChild(j + 1);
			tree[j].mDepth = tree[P - 1].mDepth + 1;

		}

		// 결과 계산 및 출력
		cout << getResultBFS() << endl;

		// 트리 초기화
		
		for (j = 0; j < tree_num; j++) {
			tree[j].init();
		}
		tree_num = 0;
		

	}

	//outFile.close();
	fclose(stream);
	//fclose(stream_out);

	return 0;
}