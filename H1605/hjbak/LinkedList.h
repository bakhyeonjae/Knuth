#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

class LinkedList {
    public:
        LinkedList();
        void set(int val);
        int get();
        int getVal(int idx);
        int length();
        void add(int val);
        
    private:
        LinkedList *next;
        int data;
        bool mSetFlag;

        void add(LinkedList *item);
        int getVal(int idx, int cnt);

    protected:
};

#endif
