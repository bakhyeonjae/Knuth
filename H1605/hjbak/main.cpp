#include "Queue.h"
#include "LinkedList.h"
#include "Graph.h"

#include <stdio.h>
#include <iostream>

#define MAX_NODE 100000

int countEdge();

int main() 
{
    int n, i, num_node;
    scanf("%d",&n);

    for ( i = 0 ; i < n ; i++ ) {
        clock_t start = clock();
        num_node = countEdge(); 
        clock_t end = clock();

        printf("%d,%lf\n",num_node,(end-start)/(double)(CLOCKS_PER_SEC/1000));
    }
    
    return 0;
}

int dist(LinkedList *a, LinkedList *b) {

    int d = 0;

    for ( int i = 0 ; i < a->length() ; i++ )
        for ( int j = 0 ; j < b->length() ; j++ ) 
            if (a->getVal(i) == b->getVal(j)) {
                d += i;
                d += j;
                return d;
            }

    return d;
}

int countEdge()
{
    Queue *queue = new Queue(MAX_NODE);
    Graph *graph = new Graph();

    int n = graph->load();   

    int e, e_prev, e_curr, i, cnt;
    cnt = 0;
    e = 0;
    e_prev = e;

    queue->enqueue(e);

    while ( queue->length() > 0 ) {
        e = queue->dequeue();

        // If you have no child, do nothing!
        if ( 0 == graph->getE(e)->length() ) continue;

        cnt += dist(graph->getP(e),graph->getP(e_prev));
        for ( i = 0 ; i < graph->getLengthOfE(e) ; i++) {
            cnt += 2;
            e_curr = graph->getEVal(e,i);
            queue->enqueue(e_curr);
        }

        // Do not go up. Just stay and calculate distance.
        cnt--;
        e_prev = e_curr;
    }

    printf("Number of edges is %d\n",cnt);

    graph->unload();

    delete queue;
    delete graph;

    return n;
}
