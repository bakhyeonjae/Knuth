#define ERROR   0 
#define SUCCESS 1

class Queue 
{
    public:
        Queue(int maxNum = 500);
        int enqueue(int node);
        int dequeue();
        int length();

        void printStatus();

    protected:
    private:
        int *data;
        int mMaxNum;
        int head;
        int tail;
};

