#include "LinkedList.h"

#include <stdio.h>

LinkedList::LinkedList()
{
    next = NULL;
    mSetFlag = false;
}

int LinkedList::getVal(int idx, int cnt) {
    
    if ( idx == cnt )
        return data;
    else
        return next->getVal(idx,cnt+1);
}

int LinkedList::getVal(int idx) {
    
    if ( 0 == idx ) return data;
    else return next->getVal(idx,1);
}

int LinkedList::length() 
{
    if (NULL == next && mSetFlag)         return 1;
    else if (NULL == next && !mSetFlag)   return 0;
    else                                  return (next->length() + 1);
}

void LinkedList::add(LinkedList *item) 
{
    if ( NULL == next )
        next = item;
    else
        next->add(item);
}

void LinkedList::add(int val) 
{
    if ( !mSetFlag) {
        set(val);
    } else {
        LinkedList *item = new LinkedList();
        item->set(val);
        add(item);
    }
}

void LinkedList::set(int val)
{
    data = val; 
    mSetFlag = true;
}

int LinkedList::get() 
{
    return data;
}
