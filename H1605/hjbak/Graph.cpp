#include "Graph.h" 
#include <stdio.h>

void Graph::unload()
{
    delete p;
    delete e;
}

int Graph::load() 
{
    int i;
    int n;

    scanf("%d",&n);
    
    p = new LinkedList[n];
    e = new LinkedList[n];

    p[0].add(0);

    for (i = 1 ; i < n ; i++ ) {
        int parent_node;
        scanf("%d",&parent_node);
        e[parent_node-1].add(i);
        p[i].add(i);
        //p[i].add(parent_node-1);
        for ( int j = 0 ; j < p[parent_node-1].length() ; j++ ) {
            p[i].add(p[parent_node-1].getVal(j));
        }
    }

    /* 
    for ( i = 0 ; i < n ; i++ ) {
        printf("length of p at %d is %d\n",i,p[i].length());
        for ( int j = 0 ; j < p[i].length() ; j++ ) {
            printf("%d,",p[i].getVal(j));
        }
        printf("\n");
    }

    for ( i = 0 ; i < n ; i++ ) {
        printf("length of e at %d is %d\n",i,e[i].length());
        for ( int j = 0 ; j < e[i].length() ; j++ ) 
            printf("%d,",e[i].getVal(j));
        printf("\n");
    }
    */

    return n;
}

LinkedList *Graph::getP(int node)
{
    return p+node;
}

LinkedList *Graph::getE(int node) {
    return e+node;
}

int Graph::getLengthOfE(int node)
{
    return e[node].length();
}

int Graph::getEVal(int node, int idx)
{
    return e[node].getVal(idx);
}
