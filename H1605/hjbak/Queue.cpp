#include "Queue.h"

#include <stdio.h>

Queue::Queue(int maxNum)
{
    mMaxNum = maxNum;

    data = new int[mMaxNum];

    head = 1;
    tail = 0;
}

int Queue::length() {
    return head-tail-1;
}

int Queue::enqueue(int node)
{
    if ( head == tail )
        return ERROR;

    data[head++] = node;

    head = (head >= mMaxNum) ? (head - mMaxNum) : head;    

    return SUCCESS;
}

int Queue::dequeue()
{
    int ret = data[++tail];

    tail = (tail >= mMaxNum) ? (tail - mMaxNum) : tail;    

    return ret;
}

void Queue::printStatus() 
{
    printf("head:%d  tail:%d\n",head,tail);
}
