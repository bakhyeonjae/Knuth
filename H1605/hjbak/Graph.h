#include "LinkedList.h"

class Graph
{
    public:
        int load();
        LinkedList *getP(int node);
        int getLengthOfE(int idx);
        int getEVal(int node, int idx);
        LinkedList *getE(int node);
        void unload();
    private:
        LinkedList *p; // parent node indices
        LinkedList *e; // edges to child nodes
    protected:
};
