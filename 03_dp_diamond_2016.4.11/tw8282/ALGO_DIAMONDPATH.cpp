#include <iostream>
using namespace std;

#define MAX_N 100

int dia[MAX_N * 2 - 1][MAX_N];
int cache[MAX_N * 2 - 1][MAX_N];

int N; // 다이아몬드 가로 길이

int max(int a, int b)
{
	if (a > b) return a;
	return b;
}

int path(int y, int x)
{
	if (x == -1) return -1;
	if ((y > N - 1) && (x >= (2 * N - y - 1))) return -1;
	if (y == (N * 2 - 2)) return dia[y][x];

	int& ret = cache[y][x];

	if (ret != -1) return ret;

	// dx 구함
	int dx = 1;

	if (y >= N - 1) dx = -1;

	return ret = max(path(y + 1, x), path(y + 1, x + dx)) + dia[y][x];
}

int main()
{
	int C; // 문제 개수

	int i;
	int x, y;

	cin >> C;

	for (i = 0; i < C; i++) {

		cin >> N;

		for (y = 0; y < N; y++) {
			for (x = 0; x <= y; x++) {
				cin >> dia[y][x];
				cache[y][x] = -1;
			}
		}

		for (; y < 2 * N - 1; y++) {
			for (x = 0; x <= 2 * N - y - 2; x++) {
				cin >> dia[y][x];
				cache[y][x] = -1;
			}
		}

		// 결과 출력
		cout << path(0, 0) << endl;


	}

	return 0;
}

