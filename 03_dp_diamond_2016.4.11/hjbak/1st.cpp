#include <stdio.h>

#define MAX_LEN 10000 // N^2

#define INVALID -1

int memo[MAX_LEN]; // table for memoization
int dat[MAX_LEN];  // data array for input 
int idxTab[199];   // contains offsets for each level
int N;             // size of data

int calcMax(int level, int index);
void initIndex();
void displayMemoTable();
void displayDiamond();
int processOneProblem();
int getLeft(int level, int index);
int getRight(int level, int index);
int getCurrentValue(int level, int index);
void initIndex();
int calcMax(int level, int index);

/**
 Displays memoization table. It's used for debugging
**/
void displayMemoTable() {
    int *inputs = memo;
    int k = 0;
    for (int i = 1 ; i <= N ; i++ ) {
        for (int j = 0 ; j < i ; j++ )
            printf("%d ", inputs[k++]);
        printf("\n");
    }
    for (int i = N-1 ; i > 0 ; i-- ) {
        for (int j = 0 ; j < i ; j++ )
            printf("%d ", inputs[k++]);
        printf("\n");
    }
}

/**
 Displays input data table. It's used for debugging
**/
void displayDiamond() {
    int *inputs = dat;
    int k = 0;
    for (int i = 1 ; i <= N ; i++ ) {
        for (int j = 0 ; j < i ; j++ )
            printf("%d ", inputs[k++]);
        printf("\n");
    }

    for (int i = N-1 ; i > 0 ; i-- ) {
        for (int j = 0 ; j < i ; j++ )
            printf("%d ", inputs[k++]);
        printf("\n");
    }

}

/**
 process one problem. 
 
 @return result of max path cost
**/
int processOneProblem()
{
    int tmp;

    scanf("%d",&N);

    int *inputs = dat;
    for ( int i = 0 ; i < N*N ; i++ ) scanf("%d",inputs++);

    initIndex();

    for ( int level = 0 ; level < N ; level++ )
        for ( int index = 0 ; index <= level ; index++ )
            memo[idxTab[level]+index] = calcMax(level,index);

    for ( int level = N, j = N ; level < 2*N-1 ; level++, j-- )
        for ( int index = 0 ; index <= j ; index++ )
            memo[idxTab[level]+index] = calcMax(level,index);

    return memo[N*N-1];
}

/**
 Get left element on upper level

 @return a positive number when it's valid, a negative number when it's not valid
**/
int getLeft(int level, int index)
{
    if (0 == level) return INVALID;

    if ( level < N ) {
        if (0 >= index) return INVALID;
        return  memo[idxTab[level-1]+index-1];
    } else {
        return  memo[idxTab[level-1]+index];
    }
}

/**
 Get right element on upper level

 @return a positive number when it's valid, a negative number when it's not valid
**/
int getRight(int level, int index)
{
    if (0 == level)     return INVALID;

    if ( level < N ) {
        if (index >= level) return INVALID;
        return  memo[idxTab[level-1]+index];
    } else {
        return  memo[idxTab[level-1]+index+1];
    }
}

/**
 Get current element.
**/
int getCurrentValue(int level, int index) {

    return  dat[idxTab[level]+index];
}

/**
 Calculate offsets for each level
**/
void initIndex() {

    int i = 0;
    idxTab[i] = 0;
    for ( i = 1 ; i < N ; i++ )     idxTab[i] = idxTab[i-1] + i;
    for ( int j = N ; j > 0 ; j--, i++ ) idxTab[i] = idxTab[i-1] + j;
   
    /*
    for ( i = 0 ; i < 2*N-1 ; i++ )
        printf("%d\n",idxTab[i]);
    */
}

/**
 Calculate a max path value and write down on memoisation table
**/
int calcMax(int level, int index) 
{
    int lval = getLeft(level,index);
    int rval = getRight(level,index);

    if (INVALID != lval && INVALID != rval) return getCurrentValue(level,index) + ((lval > rval) ? lval : rval);
    else if (INVALID != lval && INVALID == rval) return getCurrentValue(level,index) + lval;
    else if (INVALID == lval && INVALID != rval) return getCurrentValue(level,index) + rval;
    else return getCurrentValue(level,index);
}

int main()
{
    int c;

    scanf("%d",&c);

    for ( int i = 0 ; i < c ; i++ ) {
        printf("%d\n",processOneProblem());
    }
}
