#include <stdio.h>

#define MAX_LEN 20000 // N * 2N

#define INVALID 0

int memo[MAX_LEN]; // table for memoization
int dat[MAX_LEN];  // data array for input 
int idxTab[199];   // contains offsets for each level
int N;             // size of data

void displayMemoTable();
void displayDiamond();
int processOneProblem();
int getCurrentValue(int level, int index);
void initialise();

/**
 Displays memoization table. It's used for debugging
**/
void displayMemoTable() {
    int *inputs = memo;
    int k = 0;
    int line_stride = N+2;

    for (int i = 0 ; i <= 2*N+1 ; i++ ) {
        inputs = memo+i*line_stride;
        for (int j = 0 ; j <= line_stride ; j++ )
            printf("%-2d ", inputs[j]);
        printf("\n");
    }
}

/**
 Displays input data table. It's used for debugging
**/
void displayDiamond() {
    int *inputs = dat;
    int k = 0;
    int line_stride = N+2;

    for (int i = 0 ; i <= 2*N+1 ; i++ ) {
        inputs = dat+i*line_stride;
        for (int j = 0 ; j <= line_stride ; j++ )
            printf("%-2d ", inputs[j]);
        printf("\n");
    }
}

void initialise() {

    int line_stride = N+2;

    int *p_memo = memo;
    int *p_dat = dat;
    for (int i = 0 ; i < line_stride * (2*N + 1) ; i++ )  {
        *p_memo++ = 0;
        *p_dat++ = 0;
    }
}

/**
 process one problem. 
 
 @return result of max path cost
**/
int processOneProblem()
{
    int max_level;
    int *arr;
    int k;

    initialise();

    scanf("%d",&N);

    int line_stride = N+2;
    int *inputs = dat;
    for ( int l = 1 ; l <= 2*N-1 ; l++ ) {
        inputs = dat+l*line_stride;
        if (l > N) k = 2*N - l;
        else k = l;
        for ( int i = 1 ; i <= k ; i++ ) 
            scanf("%d",++inputs);        // skip 0th data element
    }

    int *l_val;
    int *r_val;
    int *p_memo;
    int *p_curr;
    int l_dir, r_dir;

    for ( int l = 1 ; l <= 2*N-1 ; l++ ) {

        if (l > N)  {
            k = 2*N - l;
            l_dir = 0;
            r_dir = 1;
        } else {       
            k = l;
            l_dir = -1;
            r_dir = 0;
        }

        int offset = l*line_stride+1;
        l_val = memo + offset - line_stride + l_dir;
        r_val = memo + offset - line_stride + r_dir;
        p_curr = dat + offset;
        p_memo = memo + offset;


        for ( int i = 1 ; i <= k ; i++ ) {
            *p_memo = *p_curr + ((*l_val > *r_val) ? *l_val : *r_val);
            p_memo++;
            p_curr++;
            l_val++;
            r_val++;
        }
    }

    return *(--p_memo);
}

int main()
{
    int c;

    scanf("%d",&c);

    for ( int i = 0 ; i < c ; i++ ) {
        printf("%d\n",processOneProblem());
    }
}
