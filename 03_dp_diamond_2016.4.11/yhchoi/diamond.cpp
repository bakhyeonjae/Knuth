#include <iostream>
#define MAX_NX 110
#define MAX_NY 220
#define MAX(a,b) (a>b) ? a:b

using namespace std;

int N;

int A[MAX_NX][MAX_NY];
int B[MAX_NX];

void init()
{
	int i, j;
	for (i = 0; i < MAX_NX; i++)
	{
		for (j = 0; j < MAX_NY; j++)
		{
			A[i][j] = 0;
		}
	}
}


int main() {

	int i, j, T, z;
	cin >> T;
	for (i = 0; i < T; i++)
	{
		init();
		cin >> N;
		cin >> A[1][1];
		for (j = 2; j <= N * 2 - 1; j++)
		{
			if (j <= N) {
				for (z = 1; z <= j; z++)
				{
					cin >> A[j][z];
					if (A[j][z] == 0) break;
					A[j][z] = MAX(A[j][z] + A[j - 1][z - 1], A[j][z] + A[j - 1][z]); //MaxSum(j, z, j - 1, z - 1, j - 1, z);
				}
			}
			else {
				for (z = 1; z <= N - (j - N); z++)
				{
					cin >> A[j][z];
					if (A[j][z] == 0) break;
					A[j][z] = MAX(A[j][z] + A[j - 1][z], A[j][z] + A[j - 1][z + 1]); //MaxSum(j, z, j - 1, z, j - 1, z + 1);
				}
			}
		}
		B[i] = A[N * 2 - 1][1];
	}

	for (i = 0; i < T; i++) cout << B[i] << endl;

	return 0;
}


