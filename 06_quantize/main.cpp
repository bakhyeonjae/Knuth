#include <iostream>

using namespace std;

#define MAX_LEN 100
#define TOO_BIG 999999999
#define NOT_ASSIGNED -1

int C;
int N; // number of sequence
int S; // quantisation level
int d[MAX_LEN];
int memo[MAX_LEN][MAX_LEN];


int T(int i, int l) {

    int ret_val = TOO_BIG;
    int interval_sum = 0;
    int tmp = 0;
    double mu = 0;
    int imu = 0;
    int interval_end;

    if ( NOT_ASSIGNED != memo[i][l] )  return memo[i][l];

    if ( l == 1 ) {

        interval_sum = 0;
        mu = 0;
        for ( int k = i ; k < N ; k++) mu += d[k];
        imu = (int)(mu / (N-i) + 0.5);
        for ( int k = i ; k < N ; k++) { interval_sum += (d[k] - imu) * (d[k] - imu); }
        tmp = interval_sum;
        if (ret_val > tmp) ret_val = tmp;

    } else {
        for (int j = i+1 ; j < N-l+1; j++) {

            interval_sum = 0;
            mu = 0;
            for ( int k = i ; k < j ; k++) mu += d[k];
            imu = (int)(mu / (j-i) + 0.5);
            for ( int k = i ; k < j ; k++) { interval_sum += (d[k] - imu) * (d[k] - imu); }
            tmp = interval_sum + T(j,l-1);
            if (ret_val > tmp) ret_val = tmp;
        }
    }

    memo[i][l] = ret_val;

    return memo[i][l];
}

void sort() {
    int tmp;

    for ( int i = i ; i < N ; i++ )
        for ( int j = i+1 ; j < N ; j++ ) 
            if ( d[i] > d[j] ) {
                tmp = d[i];
                d[i] = d[j];
                d[j] = tmp;            
            }
}

int processOneProblem() {

    cin >> N;
    cin >> S;

    if (S > N) S = N;

    for (int i = 0 ; i <= N ; i++ ) for ( int j = 0 ; j <= S ; j++ ) memo[i][j] = NOT_ASSIGNED;
    for (int i = 0 ; i < N ; i++ ) cin >> d[i];
    sort();
    return T(0,S);
}

int main() {

    cin >> C;

    for (int i = 0 ; i < C ; i++ ) 
        cout << processOneProblem() << endl;

    return 0;
}

