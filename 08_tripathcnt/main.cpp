#include <iostream>

#define MAX_LEN 100

using namespace std;

typedef struct {
    int cost;
    int cnt;
} TreePath;

int N;
TreePath tree[MAX_LEN][MAX_LEN];

int processOneProblem() {
    cin >> N;
    for (int i = 0 ; i < N ; i++)
        for (int j = 0 ; j < i+1 ; j++) {
            cin >> tree[i][j].cost;
            tree[i][j].cnt = 1;
        }

    for (int i = N-2 ; i >= 0 ; i-- )
        for (int j = 0 ; j < i+1 ; j++ ) {
            if (tree[i+1][j].cost == tree[i+1][j+1].cost) {
                tree[i][j].cost += tree[i+1][j].cost;
                tree[i][j].cnt = tree[i+1][j].cnt + tree[i+1][j+1].cnt;
            } else {
                int max_cost = (tree[i+1][j].cost > tree[i+1][j+1].cost) ? tree[i+1][j].cost : tree[i+1][j+1].cost;
                int max_cnt  = (tree[i+1][j].cost > tree[i+1][j+1].cost) ? tree[i+1][j].cnt : tree[i+1][j+1].cnt;
                tree[i][j].cost += max_cost;
                tree[i][j].cnt = max_cnt;
            }
        }

    return tree[0][0].cnt;
}

int main() {
    int C;
    cin >> C;

    for (int i = 0 ; i < C ; i++) 
        cout << processOneProblem() << endl;

    return 0;
}
