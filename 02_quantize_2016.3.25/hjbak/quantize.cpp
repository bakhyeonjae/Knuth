#include <stdio.h>

#include "quantize.h"

#define MAX_FLOAT 30000000.0f

void hierarchical_clustering();

float calcDist(Cluster *a, Cluster *b) {

    float diff = a->getCentroid() - b->getCentroid();
    return diff*diff;
}

bool findClosestPairBF(Cluster *clusters, int n, int &a, int &b) {

    float dist;
    float min = MAX_FLOAT;
    a = -1;
    b = -1;
    int i;

    for ( i = 0 ; i < n ; i++ )  {

        if ( clusters[i].isMerged() ) continue;

        for ( int j = i+1 ; j < n ; j++ ) {

            if ( clusters[j].isMerged() ) continue;

            dist = calcDist(clusters+i,clusters+j);
            if ( dist < min ) {
                a = i;
                b = j;
                min = dist;
            }
        }
    }

    return true;
}

void Item::displayData() {

    printf("%d",data);
    if ( next ) {
        printf("-");
        next->displayData();
    }
}

int Item::accumulate() {
    if ( next )
        return (data + next->accumulate());
    else
        return data;
}

float Item::calcError(float c) {
    if ( next )
        return (c-data)*(c-data) + next->calcError(c);
    else
        return (c-data)*(c-data);
}

void Cluster::addItem(Item *item) {

    if ( mLastItem ) {
        mLastItem->setNext(item);
        mLastItem = item;

    } else {
        mLastItem = item;
        mItem = item;
    }

    length++;
}

void Cluster::updateCentroid() {

    int sum = mItem->accumulate();
    
    centroid = float(sum)/length;
}

void Cluster::merge(Cluster *cluster) {

    if ( mLastItem ) {

        cluster->setMerged();
        mLastItem->setNext(cluster->getFirstItem());
        mLastItem = cluster->getLastItem();
        length += cluster->getLength();
        updateCentroid();
    }
}

float Cluster::calcError() {
    return mItem->calcError(centroid);
}

void Cluster::quantizeCentroid() {
    centroid = (int)(centroid + 0.5f);
}
