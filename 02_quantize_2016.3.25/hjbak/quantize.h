#ifndef __QUANTIZE_HEADER__
#define __QUANTIZE_HEADER__

class Item {
    public:
        Item() {
            data = 0;
            next = NULL;
        }
        Item(int value) { 
            data = value; 
            next = NULL;
        }

        int getData() { return data; }
        void setData(int value) { data = value; }
        void setNext(Item *nextItem) { next = nextItem; }
        void displayData();
        int accumulate();
        float calcError(float centroid);

    private:
        int data;
        Item *next;
};


class Cluster {
    public:
        Cluster(Item *data)  { mItem = data; mLastItem = data; length = 1;  mMergedFlag = false; }
        Cluster()  {  mItem = NULL; mLastItem = NULL; length = 0;  mMergedFlag = false; }
        void merge(Cluster *cluster);
        void addItem(Item *mItem);
        void displayData() { mItem->displayData(); }
        float getCentroid() { return centroid; }
        void updateCentroid();
        Item *getFirstItem() { return mItem; }
        Item *getLastItem() { return mLastItem; }
        int getLength() { return length; }
        void setMerged() { mMergedFlag = true; }
        bool isMerged() { return mMergedFlag; }

        void setNext(Cluster *c) { next = c; }
        float calcError();
        void quantizeCentroid();
        
    private:
        Item *mItem;
        Item *mLastItem;
        
        int length;
        float centroid;
        bool mMergedFlag;

        Cluster *next;
};

#endif
