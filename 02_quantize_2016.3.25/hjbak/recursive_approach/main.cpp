#include <stdio.h>

//int s[] = {1,2,3,2,1,2, 3, 3, 1, 1};
//int s[] = {1,2,3,4,5,6,7,8,9,10};
//int s[] = {1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,5,5,5,5,5,5,5,5,5,5,5,5,100,100};
//int s[] = {1,1,1,1,2,2,2,2,2,2,3,4,4,4,4,4};
//int s[] = {1,4,6,44,55,77,150,157,162};
int s[] = {1, 744, 755, 4, 897, 902, 890, 6, 777};
int max_val;
int min_val;
int N;
int K = 3;

int partition( int a[], int l, int r) {
    int pivot, i, j, t;
    pivot = a[l];
    i = l; j = r+1;

    while( 1)
    {
        do ++i; while( a[i] <= pivot && i <= r );
        do --j; while( a[j] > pivot );
        if( i >= j ) break;
        t = a[i]; a[i] = a[j]; a[j] = t;
    }
    t = a[l]; a[l] = a[j]; a[j] = t;
    return j;
}

void quickSort( int a[], int l, int r)
{
    int j;

    if( l < r ) 
    {
        j = partition( a, l, r);
        quickSort( a, l, j-1);
        quickSort( a, j+1, r);
    }

}

// @param c value of centroid
// @param b beginning value of error summation
// @param k number of quanization values
// 
// @return error
int calcError(int c, int b, int k) {
    
    int error = 0;

    if ( 1 == k ) {
        // accumulate the error
        for ( int i = N-1 ; s[i] >= b ; i-- ) 
            error += (c-s[i]) * (c-s[i]);

        return error;
    }

    int opt_val = 30000000;

    for ( int t = c+1 ; t <= max_val-(k-2) ; t++ ) {
        error = 0;
        for ( int i = 0 ; i < N  ; i++ ) {
            if ( s[i] <= (c+t)/2 && s[i] >= b )
                error += (c-s[i]) * (c-s[i]);
        }

        error += calcError(t,(c+t)/2+1,k-1);
        opt_val = (error < opt_val) ? error : opt_val;

    }

    return opt_val;
}

int main() {

    N = sizeof(s)/sizeof(int);
    quickSort(s,0,N-1);

    max_val = s[N-1];
    min_val = s[0];

    int minimum = calcError(min_val,min_val,K);
    int error;

    for ( int i = min_val+1 ; i < max_val ; i++ ) {
        error = calcError(i,min_val,K);
        minimum = (minimum < error) ? minimum : error;
    }

    printf("%d\n",minimum);

    return 0;
}
