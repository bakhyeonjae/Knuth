#include <stdio.h>
#include "quantize.h"

bool findClosestPairBF(Cluster *clusters, int n, int &a, int &b);

int main(int argc, char **argv) {
  
    int test_arr[] = {1,744,755,4,897,902,890,6,777};
    int n = 9;
    int cnt_cluster = n;

    Item *item = new Item[n];

    for ( int i = 0 ; i < n ; i++ ) item[i].setData(test_arr[i]);

    Cluster *clusters = new Cluster[n];

    for ( int i = 0 ; i < n ; i++ ) {
        clusters[i].addItem(item+i);
        clusters[i].updateCentroid();
    }
       
    int a,b;

    while ( cnt_cluster > 3 ) {
        if ( findClosestPairBF(clusters,n,a,b) ) {
            clusters[a].merge(clusters+b);
            cnt_cluster--;
        }
    }

    float sum = 0.0f;
    for (int i = 0 ; i < n ; i++ ) 
        if ( ! clusters[i].isMerged() )  {
            sum += clusters[i].calcError(); 
            printf("%dth centroid : %f\n",i,clusters[i].getCentroid());
        }

    printf("%d\n",(int)(sum+0.5));

    delete[] item;
    
    return 0;
}
